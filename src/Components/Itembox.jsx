import React from 'react'
import {  AiFillStar } from 'react-icons/ai';
import {MdOutlineStarOutline, MdShoppingCart} from 'react-icons/md';
import { CiHeart } from 'react-icons/ci';
import { Link } from 'react-router-dom'
const Itembox = (props) => {
  return (
    <div className='Orange'>
              <Link to ={props.Link}><img src={props.img} height='130'/></Link>
                <div className='Details'>
                    <div className='detail1'>
                    <span className='fruit'>{props.title}</span><br></br>
                    <span>₹{props.price}<MdShoppingCart className='cart2'/></span>
                </div>
                <div className='stars'>
                    <AiFillStar /><AiFillStar/><AiFillStar/><AiFillStar/><MdOutlineStarOutline className='str'/>  <CiHeart className='heart'/></div>
                </div>
              </div>
  )
}

export default Itembox;