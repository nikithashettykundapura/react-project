import React, { useState } from 'react'
import './Categories.css'
import {  AiFillStar } from 'react-icons/ai';
import {MdOutlineStarOutline} from 'react-icons/md';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebook } from '@fortawesome/free-brands-svg-icons'
import { faTwitter } from '@fortawesome/free-brands-svg-icons'
import { faInstagram } from '@fortawesome/free-brands-svg-icons'
const Categories = () => {
    const [quantity,setQuantity] = useState(1);
  const handleDecrement = () =>{
    setQuantity(prevCount => prevCount -1);
  }
  const handleIncrement = () =>{
    setQuantity(prevCount => prevCount +1);
  }
  return (
<div className="fullpage">
    <div className='Categories' id='Categories'>
      <div className="line" id='1'>
      <span style={{paddingTop:"32px"}}>Home</span>
      <ul style={{display:"flex"}}>
        <li> <b style={{fontSize:"30px"}}>•</b> Categories</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Fruits</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Orange</li>
      </ul>
      </div>
       <div className="frame1">
            <div className="frame">
                <img src='Assets/Orange3.png' alt="img1"/>
                <img src='Assets/Orange2.png' alt="img2" />
                <img src='Assets/Orange1.png' alt="img3"/>
            </div>
          
            <div className="frame2">
                <img src='Assets/Orange.png' alt=''/>
            </div>
            <div className="frame3">
                <div id="price-tag">
                    <p id="font">Fruits</p>
                    <h2>Fresh Orange Imported, 1kg</h2>
                   <div className="mrp" style={{display:"flex"}}>
                     <p id="mrp">MRP.$409.50</p>
                     <div style={{padding:"15px",fontSize:"20px"}} className='star1'><AiFillStar /><AiFillStar/><AiFillStar color/><AiFillStar/><MdOutlineStarOutline className='star'/></div>
                   </div>
                    <p>price: 298(298/kg)</p>
                    <p id="you">You Save: 27% OFF</p>
                    <p id="mrp">(inclusive of all taxes)</p>
                    <h4>Quantity</h4> 
                    <div className="Quantity">
                    <div className="btn" style={{fontSize:"30px",border:"1px black"}}><button onClick={handleDecrement}>-</button><button><span style={{padding:"0px"}}>{quantity}</span></button><button onClick={handleIncrement}>+</button></div>
                        <button id="Add1">Add to cart</button>
                    </div>
                    <button id="btn">Buy Now</button>
                    <div id="social">
                    <span style={{fontSize:"20px",padding:"15px"}}>share:</span> <FontAwesomeIcon icon={faFacebook} style={{fontSize:"35px",padding:"12px",color:"blue"}} /> <FontAwesomeIcon icon={faTwitter} style={{fontSize:"35px",  padding:"10px",color:"blue"}} />  <FontAwesomeIcon icon={faInstagram} style={{fontSize:"35px", padding:"10px",color:"blue"}} />
                    </div>
                </div>
            </div>
        </div>
        <div className="Reviews">
            <h1>Reviews</h1>
            <img src="Assets/reviews.png" alt="" className="reviews"/>
        </div>
        
    </div>
    <div className='Categories' id='Categories'>
      <div className="line">
      <span style={{paddingTop:"32px"}}>Home</span>
      <ul style={{display:"flex"}}>
        <li> <b style={{fontSize:"30px"}}>•</b> Categories</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Fruits</li>
        <li> <b style={{fontSize:"30px"}}>•</b> kiwi</li>
      </ul>
      </div>
       <div className="frame1">
            <div className="frame">
                <img src='Assets/kiwi.png' alt="img1"/>
                <img src='Assets/kiwi.png' alt="img2" />
                <img src='Assets/kiwi.png' alt="img3"/>
            </div>
          
            <div className="frame2">
                <img src='Assets/kiwi.png' alt=''/>
            </div>
            <div className="frame3">
                <div id="price-tag">
                    <p id="font">Fruits</p>
                    <h2>Fresh kiwi Imported, 1kg</h2>
                   <div className="mrp" style={{display:"flex"}}>
                     <p id="mrp">MRP.$409.50</p>
                     <div style={{padding:"15px",fontSize:"20px"}} className='star1'><AiFillStar /><AiFillStar/><AiFillStar color/><AiFillStar/><MdOutlineStarOutline className='star'/></div>
                   </div>
                    <p>price: 250(250/kg)</p>
                    <p id="you">You Save: 27% OFF</p>
                    <p id="mrp">(inclusive of all taxes)</p>
                    <h4>Quantity</h4> 
                    <div className="Quantity">
                    <div className="btn" style={{fontSize:"30px",border:"1px black"}}><button onClick={handleDecrement}>-</button><button><span style={{padding:"0px"}}>{quantity}</span></button><button onClick={handleIncrement}>+</button></div>
                        <button id="Add1">Add to cart</button>
                    </div>
                    <button id="btn">Buy Now</button>
                    <div id="social">
                    <span style={{fontSize:"20px",padding:"15px"}}>share:</span> <FontAwesomeIcon icon={faFacebook} style={{fontSize:"35px",padding:"12px",color:"blue"}} /> <FontAwesomeIcon icon={faTwitter} style={{fontSize:"35px",  padding:"10px",color:"blue"}} />  <FontAwesomeIcon icon={faInstagram} style={{fontSize:"35px", padding:"10px",color:"blue"}} />
                    </div>
                </div>
            </div>
        </div>
        <div className="Reviews">
            <h1>Reviews</h1>
            <img src="Assets/reviews.png" alt="" className="reviews"/>
        </div>
    </div>
    <div className='Categories' id='Categories'>
      <div className="line">
      <span style={{paddingTop:"32px"}}>Home</span>
      <ul style={{display:"flex"}}>
        <li> <b style={{fontSize:"30px"}}>•</b> Categories</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Fruits</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Banana</li>
      </ul>
      </div>
       <div className="frame1">
            <div className="frame">
                <img src='Assets/banana.png' alt="img1"/>
                <img src='Assets/banana.png' alt="img2" />
                <img src='Assets/banana.png' alt="img3"/>
            </div>
          
            <div className="frame2">
                <img src='Assets/banana.png' alt=''/>
            </div>
            <div className="frame3">
                <div id="price-tag">
                    <p id="font">Fruits</p>
                    <h2>Fresh Banana Imported, 3kg</h2>
                   <div className="mrp" style={{display:"flex"}}>
                     <p id="mrp">MRP.$409.50</p>
                     <div style={{padding:"15px",fontSize:"20px"}} className='star1'><AiFillStar /><AiFillStar/><AiFillStar color/><AiFillStar/><MdOutlineStarOutline className='star'/></div>
                   </div>
                    <p>price: 150(150/3kg)</p>
                    <p id="you">You Save: 27% OFF</p>
                    <p id="mrp">(inclusive of all taxes)</p>
                    <h4>Quantity</h4> 
                    <div className="Quantity">
                    <div className="btn" style={{fontSize:"30px",border:"1px black"}}><button onClick={handleDecrement}>-</button><button><span style={{padding:"0px"}}>{quantity}</span></button><button onClick={handleIncrement}>+</button></div>
                        <button id="Add1">Add to cart</button>
                    </div>
                    <button id="btn">Buy Now</button>
                    <div id="social">
                    <span style={{fontSize:"20px",padding:"15px"}}>share:</span> <FontAwesomeIcon icon={faFacebook} style={{fontSize:"35px",padding:"12px",color:"blue"}} /> <FontAwesomeIcon icon={faTwitter} style={{fontSize:"35px",  padding:"10px",color:"blue"}} />  <FontAwesomeIcon icon={faInstagram} style={{fontSize:"35px", padding:"10px",color:"blue"}} />
                    </div>
                </div>
            </div>
        </div>
        <div className="Reviews">
            <h1>Reviews</h1>
            <img src="Assets/reviews.png" alt="" className="reviews"/>
        </div>
        
    </div>
    <div className='Categories' id='Categories'>
      <div className="line">
      <span style={{paddingTop:"32px"}}>Home</span>
      <ul style={{display:"flex"}}>
        <li> <b style={{fontSize:"30px"}}>•</b> Categories</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Fruits</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Tomato</li>
      </ul>
      </div>
       <div className="frame1">
            <div className="frame">
                <img src='Assets/tomato.png' alt="img1"/>
                <img src='Assets/tomato.png' alt="img2" />
                <img src='Assets/tomato.png' alt="img3"/>
            </div>
          
            <div className="frame2">
                <img src='Assets/tomato.png' alt=''/>
            </div>
            <div className="frame3">
                <div id="price-tag">
                    <p id="font">Fruits</p>
                    <h2>Fresh Tomato Imported, 1kg</h2>
                   <div className="mrp" style={{display:"flex"}}>
                     <p id="mrp">MRP.$409.50</p>
                     <div style={{padding:"15px",fontSize:"20px"}} className='star1'><AiFillStar /><AiFillStar/><AiFillStar color/><AiFillStar/><MdOutlineStarOutline className='star'/></div>
                   </div>
                    <p>price: 100(100/kg)</p>
                    <p id="you">You Save: 27% OFF</p>
                    <p id="mrp">(inclusive of all taxes)</p>
                    <h4>Quantity</h4> 
                    <div className="Quantity">
                    <div className="btn" style={{fontSize:"30px",border:"1px black"}}><button onClick={handleDecrement}>-</button><button><span style={{padding:"0px"}}>{quantity}</span></button><button onClick={handleIncrement}>+</button></div>
                        <button id="Add1">Add to cart</button>
                    </div>
                    <button id="btn">Buy Now</button>
                    <div id="social">
                    <span style={{fontSize:"20px",padding:"15px"}}>share:</span> <FontAwesomeIcon icon={faFacebook} style={{fontSize:"35px",padding:"12px",color:"blue"}} /> <FontAwesomeIcon icon={faTwitter} style={{fontSize:"35px",  padding:"10px",color:"blue"}} />  <FontAwesomeIcon icon={faInstagram} style={{fontSize:"35px", padding:"10px",color:"blue"}} />
                    </div>
                </div>
            </div>
        </div>
        <div className="Reviews">
            <h1>Reviews</h1>
            <img src="Assets/reviews.png" alt="" className="reviews"/>
        </div>
    </div>
    <div className='Categories' id='Categories'>
      <div className="line">
      <span style={{paddingTop:"32px"}}>Home</span>
      <ul style={{display:"flex"}}>
        <li> <b style={{fontSize:"30px"}}>•</b> Categories</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Spices</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Spices</li>
      </ul>
      </div>
       <div className="frame1">
            <div className="frame">
                <img src='Assets/Spices.png' alt="img1"/>
                <img src='Assets/Spices.png' alt="img2" />
                <img src='Assets/Spices.png' alt="img3"/>
            </div>
          
            <div className="frame2">
                <img src='Assets/Spices.png' alt=''/>
            </div>
            <div className="frame3">
                <div id="price-tag">
                    <p id="font">Spices</p>
                    <h2>Fresh Spices Imported, 500g</h2>
                   <div className="mrp" style={{display:"flex"}}>
                     <p id="mrp">MRP.$409.50</p>
                     <div style={{padding:"15px",fontSize:"20px"}} className='star1'><AiFillStar /><AiFillStar/><AiFillStar color/><AiFillStar/><MdOutlineStarOutline className='star'/></div>
                   </div>
                    <p>price: 500(1000/kg)</p>
                    <p id="you">You Save: 27% OFF</p>
                    <p id="mrp">(inclusive of all taxes)</p>
                    <h4>Quantity</h4> 
                    <div className="Quantity">
                    <div className="btn" style={{fontSize:"30px",border:"1px black"}}><button onClick={handleDecrement}>-</button><button><span style={{padding:"0px"}}>{quantity}</span></button><button onClick={handleIncrement}>+</button></div>
                        <button id="Add1">Add to cart</button>
                    </div>
                    <button id="btn">Buy Now</button>
                    <div id="social">
                    <span style={{fontSize:"20px",padding:"15px"}}>share:</span> <FontAwesomeIcon icon={faFacebook} style={{fontSize:"35px",padding:"12px",color:"blue"}} /> <FontAwesomeIcon icon={faTwitter} style={{fontSize:"35px",  padding:"10px",color:"blue"}} />  <FontAwesomeIcon icon={faInstagram} style={{fontSize:"35px", padding:"10px",color:"blue"}} />
                    </div>
                </div>
            </div>
        </div>
        <div className="Reviews">
            <h1>Reviews</h1>
            <img src="Assets/reviews.png" alt="" className="reviews"/>
        </div>
        
    </div>
    <div className='Categories' id='Categories'>
      <div className="line">
      <span style={{paddingTop:"32px"}}>Home</span>
      <ul style={{display:"flex"}}>
        <li> <b style={{fontSize:"30px"}}>•</b> Categories</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Milk</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Milk</li>
      </ul>
      </div>
       <div className="frame1">
            <div className="frame">
                <img src='Assets/milk.png' alt="img1"/>
                <img src='Assets/milk.png' alt="img2" />
                <img src='Assets/milk.png' alt="img3"/>
            </div>
          
            <div className="frame2">
                <img src='Assets/milk.png' alt=''/>
            </div>
            <div className="frame3">
                <div id="price-tag">
                    <p id="font">Milk</p>
                    <h2>Fresh Milk Imported, 1l</h2>
                   <div className="mrp" style={{display:"flex"}}>
                     <p id="mrp">MRP.$409.50</p>
                     <div style={{padding:"15px",fontSize:"20px"}} className='star1'><AiFillStar /><AiFillStar/><AiFillStar color/><AiFillStar/><MdOutlineStarOutline className='star'/></div>
                   </div>
                    <p>price: 50(50/l)</p>
                    <p id="you">You Save: 27% OFF</p>
                    <p id="mrp">(inclusive of all taxes)</p>
                    <h4>Quantity</h4> 
                    <div className="Quantity">
                    <div className="btn" style={{fontSize:"30px",border:"1px black"}}><button onClick={handleDecrement}>-</button><button><span style={{padding:"0px"}}>{quantity}</span></button><button onClick={handleIncrement}>+</button></div>
                        <button id="Add1">Add to cart</button>
                    </div>
                    <button id="btn">Buy Now</button>
                    <div id="social">
                    <span style={{fontSize:"20px",padding:"15px"}}>share:</span> <FontAwesomeIcon icon={faFacebook} style={{fontSize:"35px",padding:"12px",color:"blue"}} /> <FontAwesomeIcon icon={faTwitter} style={{fontSize:"35px",  padding:"10px",color:"blue"}} />  <FontAwesomeIcon icon={faInstagram} style={{fontSize:"35px", padding:"10px",color:"blue"}} />
                    </div>
                </div>
            </div>
        </div>
        <div className="Reviews">
            <h1>Reviews</h1>
            <img src="Assets/reviews.png" alt="" className="reviews"/>
        </div>
    </div>
    <div className='Categories' id='Categories'>
      <div className="line">
      <span style={{paddingTop:"32px"}}>Home</span>
      <ul style={{display:"flex"}}>
        <li> <b style={{fontSize:"30px"}}>•</b> Categories</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Cheese</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Cheese</li>
      </ul>
      </div>
       <div className="frame1">
            <div className="frame">
                <img src='Assets/Cheese.png' alt="img1"/>
                <img src='Assets/Cheese.png' alt="img2" />
                <img src='Assets/Cheese.png' alt="img3"/>
            </div>
          
            <div className="frame2">
                <img src='Assets/Cheese.png' alt=''/>
            </div>
            <div className="frame3">
                <div id="price-tag">
                    <p id="font">Cheese</p>
                    <h2>Fresh Cheese Imported, 200g</h2>
                   <div className="mrp" style={{display:"flex"}}>
                     <p id="mrp">MRP.$409.50</p>
                     <div style={{padding:"15px",fontSize:"20px"}} className='star1'><AiFillStar /><AiFillStar/><AiFillStar color/><AiFillStar/><MdOutlineStarOutline className='star'/></div>
                   </div>
                    <p>price: 200(200/200g)</p>
                    <p id="you">You Save: 27% OFF</p>
                    <p id="mrp">(inclusive of all taxes)</p>
                    <h4>Quantity</h4> 
                    <div className="Quantity">
                    <div className="btn" style={{fontSize:"30px",border:"1px black"}}><button onClick={handleDecrement}>-</button><button><span style={{padding:"0px"}}>{quantity}</span></button><button onClick={handleIncrement}>+</button></div>
                        <button id="Add1">Add to cart</button>
                    </div>
                    <button id="btn">Buy Now</button>
                    <div id="social">
                    <span style={{fontSize:"20px",padding:"15px"}}>share:</span> <FontAwesomeIcon icon={faFacebook} style={{fontSize:"35px",padding:"12px",color:"blue"}} /> <FontAwesomeIcon icon={faTwitter} style={{fontSize:"35px",  padding:"10px",color:"blue"}} />  <FontAwesomeIcon icon={faInstagram} style={{fontSize:"35px", padding:"10px",color:"blue"}} />
                    </div>
                </div>
            </div>
        </div>
        <div className="Reviews">
            <h1>Reviews</h1>
            <img src="Assets/reviews.png" alt="" className="reviews"/>
        </div>
        
    </div>
    <div className='Categories' id='Categories'>
      <div className="line">
      <span style={{paddingTop:"32px"}}>Home</span>
      <ul style={{display:"flex"}}>
        <li> <b style={{fontSize:"30px"}}>•</b> Categories</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Fruits</li>
        <li> <b style={{fontSize:"30px"}}>•</b> kiwi</li>
      </ul>
      </div>
       <div className="frame1">
            <div className="frame">
                <img src='Assets/bottle.png' alt="img1"/>
                <img src='Assets/bottle.png' alt="img2" />
                <img src='Assets/bottle.png' alt="img3"/>
            </div>
          
            <div className="frame2">
                <img src='Assets/bottle.png' alt=''/>
            </div>
            <div className="frame3">
                <div id="price-tag">
                    <p id="font">Water Bottle</p>
                    <h2>Filter Water Imported, 1l</h2>
                   <div className="mrp" style={{display:"flex"}}>
                     <p id="mrp">MRP.$409.50</p>
                     <div style={{padding:"15px",fontSize:"20px"}} className='star1'><AiFillStar /><AiFillStar/><AiFillStar color/><AiFillStar/><MdOutlineStarOutline className='star'/></div>
                   </div>
                    <p>price: 30(30/l)</p>
                    <p id="you">You Save: 27% OFF</p>
                    <p id="mrp">(inclusive of all taxes)</p>
                    <h4>Quantity</h4> 
                    <div className="Quantity">
                    <div className="btn" style={{fontSize:"30px",border:"1px black"}}><button onClick={handleDecrement}>-</button><button><span style={{padding:"0px"}}>{quantity}</span></button><button onClick={handleIncrement}>+</button></div>
                        <button id="Add1">Add to cart</button>
                    </div>
                    <button id="btn">Buy Now</button>
                    <div id="social">
                    <span style={{fontSize:"20px",padding:"15px"}}>share:</span> <FontAwesomeIcon icon={faFacebook} style={{fontSize:"35px",padding:"12px",color:"blue"}} /> <FontAwesomeIcon icon={faTwitter} style={{fontSize:"35px",  padding:"10px",color:"blue"}} />  <FontAwesomeIcon icon={faInstagram} style={{fontSize:"35px", padding:"10px",color:"blue"}} />
                    </div>
                </div>
            </div>
        </div>
        <div className="Reviews">
            <h1>Reviews</h1>
            <img src="Assets/reviews.png" alt="" className="reviews"/>
        </div>
    </div>
    <div className='Categories' id='Categories'>
      <div className="line">
      <span style={{paddingTop:"32px"}}>Home</span>
      <ul style={{display:"flex"}}>
        <li> <b style={{fontSize:"30px"}}>•</b> Categories</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Fish</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Fish</li>
      </ul>
      </div>
       <div className="frame1">
            <div className="frame">
                <img src='Assets/fish.png' alt="img1"/>
                <img src='Assets/fish.png' alt="img2" />
                <img src='Assets/fish.png' alt="img3"/>
            </div>
          
            <div className="frame2">
                <img src='Assets/fish.png' alt=''/>
            </div>
            <div className="frame3">
                <div id="price-tag">
                    <p id="font">Fish</p>
                    <h2>Fresh Fish Imported, 1kg</h2>
                   <div className="mrp" style={{display:"flex"}}>
                     <p id="mrp">MRP.$409.50</p>
                     <div style={{padding:"15px",fontSize:"20px"}} className='star1'><AiFillStar /><AiFillStar/><AiFillStar color/><AiFillStar/><MdOutlineStarOutline className='star'/></div>
                   </div>
                    <p>price: 250(250/kg)</p>
                    <p id="you">You Save: 27% OFF</p>
                    <p id="mrp">(inclusive of all taxes)</p>
                    <h4>Quantity</h4> 
                    <div className="Quantity">
                    <div className="btn" style={{fontSize:"30px",border:"1px black"}}><button onClick={handleDecrement}>-</button><button><span style={{padding:"0px"}}>{quantity}</span></button><button onClick={handleIncrement}>+</button></div>
                        <button id="Add1">Add to cart</button>
                    </div>
                    <button id="btn">Buy Now</button>
                    <div id="social">
                    <span style={{fontSize:"20px",padding:"15px"}}>share:</span> <FontAwesomeIcon icon={faFacebook} style={{fontSize:"35px",padding:"12px",color:"blue"}} /> <FontAwesomeIcon icon={faTwitter} style={{fontSize:"35px",  padding:"10px",color:"blue"}} />  <FontAwesomeIcon icon={faInstagram} style={{fontSize:"35px", padding:"10px",color:"blue"}} />
                    </div>
                </div>
            </div>
        </div>
        <div className="Reviews">
            <h1>Reviews</h1>
            <img src="Assets/reviews.png" alt="" className="reviews"/>
        </div>
        
    </div>
    <div className='Categories' id='Categories'>
      <div className="line">
      <span style={{paddingTop:"32px"}}>Home</span>
      <ul style={{display:"flex"}}>
        <li> <b style={{fontSize:"30px"}}>•</b> Categories</li>
        <li> <b style={{fontSize:"30px"}}>•</b> dryFruits</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Anjeer</li>
      </ul>
      </div>
       <div className="frame1">
            <div className="frame">
                <img src='Assets/Anjura.png' alt="img1"/>
                <img src='Assets/Anjura.png' alt="img2" />
                <img src='Assets/Anjura.png' alt="img3"/>
            </div>
          
            <div className="frame2">
                <img src='Assets/Anjura.png' alt=''/>
            </div>
            <div className="frame3">
                <div id="price-tag">
                    <p id="font">DryFruits</p>
                    <h2>Fresh Anjura Imported, 1kg</h2>
                   <div className="mrp" style={{display:"flex"}}>
                     <p id="mrp">MRP.$409.50</p>
                     <div style={{padding:"15px",fontSize:"20px"}} className='star1'><AiFillStar /><AiFillStar/><AiFillStar color/><AiFillStar/><MdOutlineStarOutline className='star'/></div>
                   </div>
                    <p>price: 400(400/kg)</p>
                    <p id="you">You Save: 27% OFF</p>
                    <p id="mrp">(inclusive of all taxes)</p>
                    <h4>Quantity</h4> 
                    <div className="Quantity">
                    <div className="btn" style={{fontSize:"30px",border:"1px black"}}><button onClick={handleDecrement}>-</button><button><span style={{padding:"0px"}}>{quantity}</span></button><button onClick={handleIncrement}>+</button></div>
                        <button id="Add1">Add to cart</button>
                    </div>
                    <button id="btn">Buy Now</button>
                    <div id="social">
                    <span style={{fontSize:"20px",padding:"15px"}}>share:</span> <FontAwesomeIcon icon={faFacebook} style={{fontSize:"35px",padding:"12px",color:"blue"}} /> <FontAwesomeIcon icon={faTwitter} style={{fontSize:"35px",  padding:"10px",color:"blue"}} />  <FontAwesomeIcon icon={faInstagram} style={{fontSize:"35px", padding:"10px",color:"blue"}} />
                    </div>
                </div>
            </div>
        </div>
        <div className="Reviews">
            <h1>Reviews</h1>
            <img src="Assets/reviews.png" alt="" className="reviews"/>
        </div>
    </div>
    <div className='Categories' id='Categories'>
      <div className="line">
      <span style={{paddingTop:"32px"}}>Home</span>
      <ul style={{display:"flex"}}>
        <li> <b style={{fontSize:"30px"}}>•</b> Categories</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Fruits</li>
        <li> <b style={{fontSize:"30px"}}>•</b> Blue Berry</li>
      </ul>
      </div>
       <div className="frame1">
            <div className="frame">
                <img src='Assets/Blueberry.png' alt="img1"/>
                <img src='Assets/Blueberry.png' alt="img2" />
                <img src='Assets/Blueberry.png' alt="img3"/>
            </div>
          
            <div className="frame2">
                <img src='Assets/Blueberry.png' alt=''/>
            </div>
            <div className="frame3">
                <div id="price-tag">
                    <p id="font">Fruits</p>
                    <h2>Fresh Blue Berry Imported, 1kg</h2>
                   <div className="mrp" style={{display:"flex"}}>
                     <p id="mrp">MRP.$409.50</p>
                     <div style={{padding:"15px",fontSize:"20px"}} className='star1'><AiFillStar /><AiFillStar/><AiFillStar color/><AiFillStar/><MdOutlineStarOutline className='star'/></div>
                   </div>
                    <p>price: 250(250/kg)</p>
                    <p id="you">You Save: 27% OFF</p>
                    <p id="mrp">(inclusive of all taxes)</p>
                    <h4>Quantity</h4> 
                    <div className="Quantity">
                    <div className="btn" style={{fontSize:"30px",border:"1px black"}}><button onClick={handleDecrement}>-</button><button><span style={{padding:"0px"}}>{quantity}</span></button><button onClick={handleIncrement}>+</button></div>
                        <button id="Add1">Add to cart</button>
                    </div>
                    <button id="btn">Buy Now</button>
                    <div id="social">
                    <span style={{fontSize:"20px",padding:"15px"}}>share:</span> <FontAwesomeIcon icon={faFacebook} style={{fontSize:"35px",padding:"12px",color:"blue"}} /> <FontAwesomeIcon icon={faTwitter} style={{fontSize:"35px",  padding:"10px",color:"blue"}} />  <FontAwesomeIcon icon={faInstagram} style={{fontSize:"35px", padding:"10px",color:"blue"}} />
                    </div>
                </div>
            </div>
        </div>
        <div className="Reviews">
            <h1>Reviews</h1>
            <img src="Assets/reviews.png" alt="" className="reviews"/>
        </div>
    </div>
</div>
  )
}

export default Categories