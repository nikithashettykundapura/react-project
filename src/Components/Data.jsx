import Orange from './Assets/Orange.png';
import kiwi from './Assets/kiwi.png';
import banana from './Assets/banana.png';
import tomato from './Assets/tomato.png';
import Spices from './Assets/Spices.png';
import milk from './Assets/milk.png';
import Cheese from './Assets/Cheese.png';
import bottle from './Assets/bottle.png';
const Data = {
    productdata:[
        {
            id: 1,
            img:Orange,
            name: 'Orange -1kg',
            price: '298.00',
            Link: '/orange'
        },
        {
            id: 2,
            img: kiwi,
            name: 'kiwi -1kg',
            price: '250.00',
            Link:'/kiwi'
        },
        {
            id: 3,
            img: banana,
            name: 'Banana -3kg',
            price: '150.00',
            Link:'/banana'
        },
        {
            id: 4,
            img: tomato,
            name: 'Tomato-1kg',
            price: '100.00',
            Link:'/tomato'
        },
        {
            id: 5,
            img: Spices,
            name: 'Spices -500g',
            price: '500.00',
            Link:'/spices'
        },
        {
            id: 6,
            img: milk,
            name: 'Milk -2l',
            price: '100.00',
            Link:'/milk'
        },
        {
            id: 7,
            img: Cheese,
            name: 'Cheese -200g',
            price: '200.00',
            Link:'/cheese'
        },
        {
            id: 8,
            img: bottle,
            name: 'Water -1l',
            price: '30.00',
            Link:'/bottle'
        },

    ],
};

export default Data;