import React, { useState } from 'react'
import {  AiFillStar } from 'react-icons/ai';
import {MdOutlineStarOutline} from 'react-icons/md';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebook } from '@fortawesome/free-brands-svg-icons'
import { faTwitter } from '@fortawesome/free-brands-svg-icons'
import { faInstagram } from '@fortawesome/free-brands-svg-icons'
const Milk = () => {
  const [quantity,setQuantity] = useState(1);
  const handleDecrement = () =>{
    setQuantity(prevCount => prevCount -1);
  }
  const handleIncrement = () =>{
    setQuantity(prevCount => prevCount +1);
  }
  return (
    <div className='Categories' id='Categories'>
    <div className="line">
    <span style={{paddingTop:"32px"}}>Home</span>
    <ul style={{display:"flex"}}>
      <li> <b style={{fontSize:"30px"}}>•</b> Categories</li>
      <li> <b style={{fontSize:"30px"}}>•</b> Milk</li>
      <li> <b style={{fontSize:"30px"}}>•</b> Milk</li>
    </ul>
    </div>
     <div className="frame1">
          <div className="frame">
              <img src='Assets/milk.png' alt="img1"/>
              <img src='Assets/milk.png' alt="img2" />
              <img src='Assets/milk.png' alt="img3"/>
          </div>
        
          <div className="frame2">
              <img src='Assets/milk.png' alt=''/>
          </div>
          <div className="frame3">
              <div id="price-tag">
                  <p id="font">Milk</p>
                  <h2>Fresh Milk Imported, 1l</h2>
                 <div className="mrp" style={{display:"flex"}}>
                   <p id="mrp">MRP.$409.50</p>
                   <div style={{padding:"15px",fontSize:"20px"}} className='star1'><AiFillStar /><AiFillStar/><AiFillStar color/><AiFillStar/><MdOutlineStarOutline className='star'/></div>
                 </div>
                  <p>price: 50(50/l)</p>
                  <p id="you">You Save: 27% OFF</p>
                  <p id="mrp">(inclusive of all taxes)</p>
                  <h4>Quantity</h4> 
                  <div className="Quantity">
                  <div className="btn" style={{fontSize:"30px",border:"1px black"}}><button onClick={handleDecrement}>-</button><button><span style={{padding:"0px"}}>{quantity}</span></button><button onClick={handleIncrement}>+</button></div>
                      <button id="Add1">Add to cart</button>
                  </div>
                  <button id="btn">Buy Now</button>
                  <div id="social">
                  <span style={{fontSize:"20px",padding:"15px"}}>share:</span> <FontAwesomeIcon icon={faFacebook} style={{fontSize:"35px",padding:"12px",color:"blue"}} /> <FontAwesomeIcon icon={faTwitter} style={{fontSize:"35px",  padding:"10px",color:"blue"}} />  <FontAwesomeIcon icon={faInstagram} style={{fontSize:"35px", padding:"10px",color:"blue"}} />
                  </div>
              </div>
          </div>
      </div>
      <div className="Reviews">
          <h1>Reviews</h1>
          <img src="Assets/reviews.png" alt="" className="reviews"/>
      </div>
  </div>
  )
}

export default Milk