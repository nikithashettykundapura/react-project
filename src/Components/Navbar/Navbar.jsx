import React, { useState } from 'react';
import './Navbar.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faHeart, faCartShopping } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';

const Navbar = () => {
  const [menu, setMenu] = useState("Shop");

  return (
    <div className='navbar'>
      <p className='nav-logo'><i>DailyDelightMart</i></p>
      <div className="search-box">
        <input type="text" placeholder='Search'/>
        <FontAwesomeIcon icon={faSearch} style={{color: "green"}}/>
      </div>
      <ul className='nav-menu'>
        <li onClick={() => setMenu("Shop")}>
          <Link to='/' className={menu === "Shop" ? "active" : ""}>Home</Link>
          {menu === "Shop" ? <hr/> : null}
        </li>
        <li onClick={() => setMenu("Categories")}>
          <Link to='/category' className={menu === "Categories" ? "active" : ""}>Categories</Link>
          {menu === "Categories" ? <hr/> : null}
        </li>
        <li onClick={() => setMenu("Checkout")}>
          <Link to='/cart' className={menu === "Checkout" ? "active" : ""}>Checkout</Link>
          {menu === "Checkout" ? <hr/> : null}
        </li>
      </ul>
      <div className='hrt'><FontAwesomeIcon icon={faHeart} style={{color: "green",fontSize: "25px"}}/></div>
      <div className='crt'><FontAwesomeIcon icon={faCartShopping} style={{color: "green",fontSize: "25px"}} /></div>
      <div className="nav-cart-count">0</div>
    <Link to='/login'>  <button className='login'>Log In</button></Link>
      <button className='register'>Register</button>
    </div>
  )
}

export default Navbar;
