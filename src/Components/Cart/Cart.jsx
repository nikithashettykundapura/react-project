import React, { useState } from 'react'
import './Cart.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash} from '@fortawesome/free-solid-svg-icons'; 
const Cart = () => {
  const [quantity,setQuantity] = useState(1);
  const handleDecrement = () =>{
    setQuantity(prevCount => prevCount -1);
  }
  const handleIncrement = () =>{
    setQuantity(prevCount => prevCount +1);
  }
  const [quantity1,setQuantity1] = useState(1);
  const handleDecrement1 = () =>{
    setQuantity1(prevCount => prevCount -1);
  }
  const handleIncrement1 = () =>{
    setQuantity1(prevCount => prevCount +1);
  }
  const [quantity2,setQuantity2] = useState(1);
  const handleDecrement2 = () =>{
    setQuantity2(prevCount => prevCount -1);
  }
  const handleIncrement2 = () =>{
    setQuantity2(prevCount => prevCount +1);
  }
  return (
    <div className='Cart'>
       <h4 style={{fontSize:"25px",paddingLeft:"150px"}}>Shopping Cart (3)</h4>
       <div className="cart-page">
            <div className="cart-items">
                <div className="produce">
                    <div className="produce-img">
                    <img src='Assets/Orange.png' alt='orange' height='100'/>
                    </div>
                    <div className="text2">
                      <p>Fresh mini orange -1Kg</p>
                      <span><b>₹</b> 298.00</span>
                    </div>
                    <div className="add" style={{padding:"40px",marginLeft:"55px"}}>
                    <button onClick={handleDecrement} style={{margin:"2px",borderRadius:"50%",fontSize:"20px"}}>-</button><span style={{fontSize:"20px",padding:"0px"}}>{quantity}</span><button onClick={handleIncrement} style={{margin:"2px",borderRadius:"50%",fontSize:"20px"}}>+</button>
                    </div>
                    <div className="delete" style={{padding:"40px",marginLeft:"80px"}}>
                      <FontAwesomeIcon icon={faTrash} style={{fontSize:"20px"}}/>
                    </div>
                </div>
                <div className="produce">
                    <div className="produce-img">
                    <img src='Assets/Apple.png' alt='apple' height='100'/>
                    </div>
                    <div className="text2">
                      <p>Apple -1Kg</p>
                      <span><b>₹</b> 213.00</span>
                    </div>
                    <div className="add" style={{padding:"40px",marginLeft:"180px"}}>
                      <button onClick={handleDecrement1} style={{margin:"2px",borderRadius:"50%",fontSize:"20px"}}>-</button><span style={{fontSize:"20px",padding:"0px"}}>{quantity1}</span><button onClick={handleIncrement1} style={{margin:"2px",borderRadius:"50%",fontSize:"20px"}}>+</button>
                    </div>
                    <div className="delete" style={{padding:"40px",marginLeft:"80px"}}>
                      <FontAwesomeIcon icon={faTrash} style={{fontSize:"20px"}}/>
                    </div>
                </div>
                <div className="produce">
                    <div className="produce-img">
                    <img src='Assets/fish.png' alt='orange' height='100'/>
                    </div>
                    <div className="text2">
                      <p>Fresh Fish</p>
                      <span><b>₹</b> 250.00</span>
                    </div>
                    <div className="add" style={{padding:"40px",marginLeft:"170px"}}>
                    <button onClick={handleDecrement2} style={{margin:"2px",borderRadius:"50%",fontSize:"20px"}}>-</button><span style={{fontSize:"20px",padding:"0px"}}>{quantity2}</span><button onClick={handleIncrement2} style={{margin:"2px",borderRadius:"50%",fontSize:"20px"}}>+</button>
                    </div>
                    <div className="delete" style={{padding:"40px",marginLeft:"80px"}}>
                      <FontAwesomeIcon icon={faTrash} style={{fontSize:"20px"}}/>
                    </div>
                </div>
            </div>
            <div className="shipping">
              <h4 style={{fontSize:"25px",paddingLeft:"5px"}}>Shipping</h4>
              <div className="ship">
                <div className="shipment">
                <p style={{paddingRight:"150px"}}>Subtotal</p>
                <span style={{paddingTop:"15px"}}><b>₹</b> 763.00</span>
                </div>
                <div className="shipment">
                <p style={{paddingRight:"150px"}}>Delivery</p>
                <span style={{paddingTop:"15px"}}><b>₹</b> 0.00</span>
                </div>
                <div className="shipment">
                <p style={{paddingRight:"170px"}}>Total</p>
                <span style={{paddingTop:"15px"}}><b>₹</b> 763.00</span>
                </div>
                <button className='button'>Proceed To checkout</button>
              </div>
            </div>
       </div>
    </div>
  )
}

export default Cart