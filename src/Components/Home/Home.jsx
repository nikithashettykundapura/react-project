import React from 'react';
import './Home.css';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeadphones } from '@fortawesome/free-solid-svg-icons';
import { faShieldAlt } from '@fortawesome/free-solid-svg-icons';
import { faRotate } from '@fortawesome/free-solid-svg-icons';
import { faTruckFast } from '@fortawesome/free-solid-svg-icons/faTruckFast';
import {  AiFillStar } from 'react-icons/ai';
import {MdOutlineStarOutline, MdShoppingCart} from 'react-icons/md';
import { faLocationDot } from '@fortawesome/free-solid-svg-icons'
import { faSquareEnvelope } from '@fortawesome/free-solid-svg-icons'
import { faFacebook } from '@fortawesome/free-brands-svg-icons'
import { faTwitter } from '@fortawesome/free-brands-svg-icons'
import { faInstagram } from '@fortawesome/free-brands-svg-icons'
// import { CiHeart } from 'react-icons/ci';
import Itembox from '../Itembox';
import Data from '../Data';
const Home = () => {
  return (
  <div className="Hero">
    <div className="home">
      <div className="home-content" id='home'>
        <div className='box' style={{ background: 'rgb(245 245 245/40%)', padding: '30px' }}>
          <h1>Let's now shop For<br></br> daily food & necessary.</h1>
          <p>Freshness and speed at your Fingertips:<br></br>
            Get groceries delivered in 30 Minutes with our website or mobile app.</p>
          <a href='#items'>
            <button>Shop Now</button>
          </a>
        </div>
      </div>
      <div className="text">
        <div className="text1">
          <div className='headphone'><FontAwesomeIcon icon={faHeadphones} style={{ color: "green", fontSize: "40px", padding: "20px" }} /></div>
          <div>
            <p><b>Support 24 h</b><br />
              Dedicated support</p>
          </div>
        </div>
        <div className="text1">
          <div className='shield'><FontAwesomeIcon icon={faShieldAlt} style={{ color: "green", fontSize: "40px", padding: "20px" }} /></div>
          <div>
            <p><b>Secure Payment</b><br />
              ensure your money is safe</p>
          </div>
        </div>
        <div className="text1">
          <div className='refund'><FontAwesomeIcon icon={faRotate} style={{ color: "green", fontSize: "40px", padding: "20px" }} /></div>
          <div>
            <p><b>Refundable</b><br />
              Damage items can be refunded</p>
          </div>
        </div>
        <div className="text1">
          <div className='shipping'><FontAwesomeIcon icon={faTruckFast} style={{ color: "green", fontSize: "40px", padding: "20px" }} /></div>
          <div>
            <p><b>Free Shipping</b><br />
              Free Shipping over  ₹500</p>
          </div>
        </div>
      </div>
    </div>
    <div className="page" id="Offers">
    <div className='Offer' >
        <div className='veges'>
            <h1 >FRESH VEGETABLES<br></br>BIG SALE</h1>
         <img  className='img1' src='Assets/Vegetables 2.png' alt='vegetables' height={200}/>
        </div>
        <div className='Offer1'>
          <div className='Fruit'>
          <img  className='img2' src='Assets/fruits 2.png' alt='fruit' height={150} />
          <div className='Inform'>
          <h1>FRESH  FRUIT  SUPER  SALE</h1>
          <button>shop now</button>
          </div>
          </div>
          <div className='Juice'>
            <div className='Inform'>
              <img src='Assets/Frame 30.png' alt='discount' height={80}/>
              <h2>ENJOY YOUR FRESH DRINK</h2>
            </div>
          <img  className='img3' src='Assets/juice.png' alt='fruit' height={190} />
          </div>
        </div>
      </div>  
        <div className="flashsales">
          <div className="hr">
            <hr style={{width:"400px",margin:"40px",height:"3px",backgroundColor:"rgb(21, 21, 83)"}}></hr>
          </div>
          <div className="text3">
            <p style={{fontSize:"25px",fontWeight:600}}>Flash Sales</p>
          </div>
          <div className="hr">
          <hr style={{width:"400px",margin:"40px",height:"3px",backgroundColor:"rgb(21, 21, 83)"}}></hr>
          </div>
        </div> 
        <div className='duration'>
           <div className='days'>
            <h2>02</h2>
            <h3>DAYS</h3>
            </div>
            <div className='hours'>
                <h2>14</h2>
                <h3>HOURS</h3>
            </div>
            <div className='minutes'>
                <h2>26</h2>
                <h3>MINS</h3>
            </div>
        </div>
    </div>
    <div className='items' id='items'>
            <div className='fish'>  
              <Link to='/fish'>  <img src='Assets/fish.png' alt='meat' height='100'/></Link>
                <div className='abt'>
                   <div className='star1'>
                    <AiFillStar /><AiFillStar/><AiFillStar color/><AiFillStar/><MdOutlineStarOutline className='str'/> </div>
                    <div className='detail'>
                    <span>Fresh Fish</span><br></br>
                    <span style={{fontWeight:"bold"}}>₹250.00<MdShoppingCart className='cart'/></span>
                </div>
             </div>
            </div>

            <div className='Anjura'>
              <Link to='/anjura'>  <img src='Assets/Anjura.png' alt='dryfruits' height='110'/></Link>
                <div className='abt'>
                   <div className='star1'>
                    <AiFillStar /><AiFillStar/><AiFillStar color/><AiFillStar/><MdOutlineStarOutline className='str'/> </div>
                    <div className='detail'>
                    <span>Anjeer</span><br></br>
                    <span style={{fontWeight:"bold"}}>₹400.00<MdShoppingCart className='cart1'/></span>
                </div>
             </div>
            </div>

            <div className='berry'>
                <Link to='/blue'><img src='Assets/Blueberry.png' alt='berry' height='110'/></Link>
                <div className='abt'>
                   <div className='star1'>
                    <AiFillStar /><AiFillStar/><AiFillStar color/><AiFillStar/><MdOutlineStarOutline className='str'/> </div>
                    <div className='detail'>
                    <span>blueberry</span><br></br>
                    <span style={{fontWeight:"bold"}}>₹250.00<MdShoppingCart className='cart2'/></span>
                </div>
             </div>
            </div>
    </div>
    <div className='bestdeals'>
              <h1>Daily Best Sell </h1>
               <div className='group1'>
                {Data.productdata.map((item,index)=>{
                  return(<Itembox img={item.img}  title={item.name} Link={item.Link} price={item.price} item={item} key={index}/>)
                })}
              </div>
       </div>
       <div class="contactus" id="contactus">
        <div class="Contact1">
            <span> <a style={{fontSize:"25px"}}>Contact Us</a><br/>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates laudantium corrupti.</p>
            <FontAwesomeIcon icon={faLocationDot} style={{color: "#fff",fontSize:"25px"}} />  <a>Bo Street, West</a><br/><br/>
            <FontAwesomeIcon icon={faSquareEnvelope} style={{color: "#fff",fontSize:"25px"}} />  <a>Info@eg.com</a><br/>
            </span>
        </div>
        <div class="Contact2">
            <p className='con-logo'><i>DailyDelightMart</i></p>
            <p class="Meta">COPYRIGHT  ©: 2024, ALL RIGHTS RESERVED</p>
            <FontAwesomeIcon icon={faFacebook} style={{color: "#fff",fontSize:"35px",padding:"10px"}} /> <FontAwesomeIcon icon={faTwitter} style={{color: "#fff",fontSize:"35px",  padding:"10px"}} />  <FontAwesomeIcon icon={faInstagram} style={{color: "#fff",fontSize:"35px", padding:"10px"}} />
        </div>
        <div class="Contact3" id='Contact3'>
            <span> <a>INFORMATION</a><br/><br/>
            <a>About us</a><br/>
            <a>Contact us</a><br/>
            <a>Help and advice</a><br/>
            <a>Store Locations</a><br/>
            <a>FAQs</a><br/>
            </span>
        </div>
    </div>
  </div>  
  );
};

export default Home;
