import React from 'react'
import { Link } from 'react-router-dom'
const Login = () => {
  return (
    <div className="loginsignup">
      <div className="loginsignup-container">
        <h1>Log In</h1>
        <div className="loginsignup-fields">
          <input type="email" placeholder='Email Address' />
          <input type="password" placeholder='Password' />
        </div>
        <button>Continue</button>
        <p className="loginsignup-login">Don't have an account?<Link to ='/login'  style={{textDecoration:"none"}}><span>Sign Up here</span></Link></p>
        <div className="loginsignup-agree">
        </div>
      </div>
    </div>
  )
}

export default Login