import React from 'react';
import { Routes, Route } from 'react-router-dom';
import './App.css';
import Navbar from './Components/Navbar/Navbar';
import Cart from './Components/Cart/Cart';
import Orange from './Components/Orangedata';
import Kiwi from './Components/Kiwi';
import Banana from './Components/Banana';
import Tomato from './Components/Tomato';
import Spices from './Components/Spices';
import Milk from './Components/Milk';
import Cheese from './Components/Cheese';
import Bottle from './Components/Bottle';
import Fish from './Components/Fish';
import Anjura from './Components/Anjura';
import Blue from './Components/Blue';
import LoginSignup from './Components/LoginSignup/LoginSignup';
import Login from './Components/Login/Login';
import Home from './Components/Home/Home';
import Categories from './Components/Categories/Categories';
function App() {
  return (
    <div>
      <Navbar />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/category' element={<Categories />} />
        <Route path='/login' element={<LoginSignup />} /> 
        <Route path='/sign' element={<Login/>}/>
        <Route path='/cart' element={<Cart />} />
        <Route path='/orange' element={<Orange/>} />
        <Route path='/kiwi' element={<Kiwi/>} />
        <Route path='/banana' element={<Banana/>} />
        <Route path='/tomato' element={<Tomato/>} />
        <Route path='/spices' element={<Spices/>} />
        <Route path='/milk' element={<Milk/>} />
        <Route path='/cheese' element={<Cheese/>} />
        <Route path='/bottle' element={<Bottle/>} />
        <Route path='/bottle' element={<Bottle/>} />
        <Route path='/fish' element={<Fish/>} />
        <Route path='/anjura' element={<Anjura/>} />
        <Route path='/blue' element={<Blue/>} />
      </Routes>
    </div>
  );
}

export default App;
